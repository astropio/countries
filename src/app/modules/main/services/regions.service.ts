import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, debounceTime, map, Observable, shareReplay, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RegionModel } from '../models/region.model';

@Injectable()
export class RegionsService {

  getAllRegions(){
    return this.http
      .get<RegionModel[]>(environment.api + 'all?fields=region')
  }
 
  constructor(private http: HttpClient) { }

}
