import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { MainComponent } from './main.component';
import { RegionComponent } from './region/region.component';
import { RegionsService } from './services/regions.service';
import { MainRoutingModule } from './main-routing.module';
import { EffectsModule } from '@ngrx/effects';
import { RegionsEffects } from './store/regions.effects';
import { StoreModule } from '@ngrx/store';
import { regionsReducer } from './store/regions.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



@NgModule({
  declarations: [
    MainComponent,
    RegionComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MainRoutingModule,
    EffectsModule.forFeature([RegionsEffects]),
    StoreModule.forFeature('regionsState', regionsReducer),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    RegionsService
  ]
})
export class MainModule { }
