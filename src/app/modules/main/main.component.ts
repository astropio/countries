import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable, tap } from 'rxjs';
import { RegionModel } from './models/region.model';
import { RegionsService } from './services/regions.service';
import { ShowAllAction } from './store/regions.actions';
import { getMessage, getRegions } from './store/regions.selectors';
import { RegionsState } from './store/regions.state';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  regions$: Observable<{name: string, link: string}[]>;

  constructor(
    private store: Store<RegionsState>
    ) {
      this.regions$ = store.select(getRegions);
    }

  ngOnInit(): void {
    this.store.dispatch(ShowAllAction())
  }

}
