import { createAction, props } from "@ngrx/store";
import { RegionModel } from "../models/region.model";

export const ShowAllAction = createAction('[REGION] Show All');
export const ShowAllSuccessAction = createAction('[REGION] Show All Success', props<{ payload: RegionModel[]}>());
