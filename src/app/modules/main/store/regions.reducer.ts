import { Action, createReducer, on } from "@ngrx/store";
import { RegionsState } from "./regions.state";
import * as fromActions from './regions.actions'

export const initialState: RegionsState = {regions: [], message: ''};

export const regionsReducer = createReducer(
    initialState,
    on(fromActions.ShowAllSuccessAction, (state, {payload}) => ({regions: payload, message: 'Success'})),
);
