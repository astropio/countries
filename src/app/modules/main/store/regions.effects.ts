import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, EMPTY, map, mergeMap, of, switchMap, tap } from "rxjs";
import { RegionModel } from "../models/region.model";
import { RegionsService } from "../services/regions.service";
import * as fromActions from "./regions.actions"

@Injectable()
export class RegionsEffects{

    regionsCache: RegionModel[] = []

    constructor(
        private actions$: Actions,
        private regionsService: RegionsService
      ) { }
    
    loadAllRegions = createEffect(() => 
        this.actions$.pipe(
            ofType(fromActions.ShowAllAction),
            switchMap(() => {
                if(! (this.regionsCache.length > 0))
                    return this.regionsService.getAllRegions().pipe(
                        map(data =>  [... new Set(data.map(d => d.region))].map<RegionModel>(str =>{ return {region: str}})),
                        tap(data => this.regionsCache = data),
                        map(data => fromActions.ShowAllSuccessAction({payload: data}))
                    )
                else
                    return of(fromActions.ShowAllSuccessAction({payload: this.regionsCache}))
                }
            )
        )
    )
    
}