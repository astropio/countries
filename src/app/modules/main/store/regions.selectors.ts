import { createFeatureSelector, createSelector } from "@ngrx/store";
import { RegionsState } from "./regions.state";

export const getRegionsState = createFeatureSelector<RegionsState>('regionsState');

export const getRegions = createSelector(
    getRegionsState,
    (state: RegionsState) => state?.regions?.map((reg) => {
        return {name: reg.region, link: reg.region.toLowerCase()}
    })
);

export const getMessage = createSelector(
    getRegionsState,
    (state: RegionsState) => state.message
);
