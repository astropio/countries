import { RegionModel } from "../models/region.model";

export interface AppState {
	regionsState: RegionModel;
}

export interface RegionsState {
	regions: RegionModel[];
	message: any;
} 