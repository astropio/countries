import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class ImageService {

  constructor() { }

  getImageLink(code: string): string{
    return environment.flagsApi + code;
  }
}
