import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ImageService } from '../shared/services/image.service';
import { CountryModel } from './models/country.model';
import { ShowAction } from './store/country.actions';
import { getCountry } from './store/country.selectors';
import { CountryState } from './store/country.state';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {

  country$: Observable<CountryModel>;
  imageLink: string = '';
  
  constructor(
    private route: ActivatedRoute,
    private imageService: ImageService,
    private store: Store<CountryState>

  ) {
    this.country$ = store.select(getCountry);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(ShowAction({payload: {code: params['code']}}))
      this.getImageLink();
    });
  }

  getImageLink(){
    this.country$.subscribe((country) => {
      if(country && country.code != 'brak')
        this.imageLink = this.imageService.getImageLink(country.code);
    })
  }
}

