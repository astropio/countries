import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CountryState } from "./country.state";

export const getCountryState = createFeatureSelector<CountryState>('countryState');

export const getCountry = createSelector(
    getCountryState,
    (state: CountryState) => state.country
);

