import { createAction, props } from "@ngrx/store";
import { CountryModel } from "../models/country.model";

export const ShowAction = createAction('[Country] Show', props<{payload: {code: string}}>());
export const ShowSuccessAction = createAction('[Country] Show Success', props<{ payload: {country: CountryModel}}>());
