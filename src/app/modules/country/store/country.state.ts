import { CountryModel } from "../models/country.model";

export interface AppState {
	countryState: CountryState;
}

export interface CountryState {
	country: CountryModel;
	message: any;
} 