import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, EMPTY, map, mergeMap, Observable, of, switchMap } from "rxjs";
import { CountryModel } from "../models/country.model";
import { CountryService } from "../services/country.service";
import * as fromActions from "./country.actions"

@Injectable()
export class CountryEffects{

  countryCache: {
    [key: string]: CountryModel
  } = {}

    constructor(
        private actions$: Actions,
        private countryService: CountryService
      ) { }
    
    loadCountry = createEffect(() => 
        this.actions$.pipe(
            ofType(fromActions.ShowAction),
            switchMap(action => {
              
                if(!this.countryCache[action.payload.code])
                  return this.countryService.getCountry(action.payload.code).pipe(
                      map(country => 
                          {
                            let cc=  {
                              capital: country.capital.length > 0 ? country.capital[0] : undefined, 
                              code: country.cca3, 
                              name: country.name.common, 
                              size: country.area + '', 
                              currency: Object.keys(country.currencies).length > 0 ? {
                                code: country.currencies[Object.keys(country.currencies)[0]].symbol,
                                name: country.currencies[Object.keys(country.currencies)[0]].name
                              } : undefined
                            } as CountryModel
                            this.countryCache[action.payload.code] = cc;

                            return cc;
                          }
                        ),
                      map(data => fromActions.ShowSuccessAction({payload: {country: data}}))
                  )
                else
                  return of(fromActions.ShowSuccessAction({payload: {country: this.countryCache[action.payload.code]}}))
              }
            )
        )
    )
    
}