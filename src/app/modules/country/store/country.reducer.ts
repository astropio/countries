import { createReducer, on } from "@ngrx/store";
import { CountryState } from "./country.state";
import * as fromActions from './country.actions'

export const initialState: CountryState = {country: {
    code: 'brak',
    name: 'brak',
    size: 'brak',
}, message: ''};

export const countryReducer = createReducer(
    initialState,
    on(fromActions.ShowSuccessAction, (state, {payload}) => ({country: payload.country, message: 'Success'})),
);
