import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryComponent } from './country.component';
import { CountryRoutingModule } from './country-rounting.module';
import { CountryService } from './services/country.service';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { CountryEffects } from './store/country.effects';
import { StoreModule } from '@ngrx/store';
import { countryReducer } from './store/country.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



@NgModule({
  declarations: [
    CountryComponent
  ],
  imports: [
    CommonModule,
    CountryRoutingModule,
    HttpClientModule,
    EffectsModule.forFeature([CountryEffects]),
    StoreModule.forFeature('countryState', countryReducer),
    StoreDevtoolsModule.instrument()
  ],
  providers:[
    CountryService
  ]
})
export class CountryModule { }
