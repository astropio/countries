export interface GetCountryModel{
    capital: string[];
    cca3: string;
    name: {
        common: string, 
        nativeName: any, 
        official: string 
    };
    currencies: {[key: string]: {name: string, symbol:string}};
    area: number;
}