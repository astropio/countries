export interface CountryModel{
    name: string;
    capital?: string;
    currency?: {code: string, name: string};
    size: string;
    code: string
}