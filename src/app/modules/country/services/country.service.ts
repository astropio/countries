import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { debounceTime, map, Observable, shareReplay, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CountryModel } from '../models/country.model';
import { GetCountryModel } from '../models/get-country.model';

@Injectable()
export class CountryService {


  constructor(private http: HttpClient) { }

  getCountry(code: string): Observable<GetCountryModel>{
    return this.http
      .get<GetCountryModel>(environment.api + 'alpha/' + code + '?fields=name,cca3,currencies,capital,area')

  }

}
