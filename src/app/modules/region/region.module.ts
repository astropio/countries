import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegionComponent } from './region.component';

import {OrderListModule} from 'primeng/orderlist';
import {TableModule} from 'primeng/table';


import { HttpClientModule } from '@angular/common/http';
import { RegionRoutingModule } from './region-routing.module';
import { CountriesService } from './services/countries.service';
import { RippleModule } from 'primeng/ripple';
import { EffectsModule } from '@ngrx/effects';
import { CountriesEffects } from './store/region.effects';
import { StoreModule } from '@ngrx/store';
import { countriesReducer } from './store/region.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



@NgModule({
  declarations: [
    RegionComponent
  ],
  imports: [
    CommonModule,
    OrderListModule,
    HttpClientModule,
    RegionRoutingModule,
    TableModule,
    EffectsModule.forFeature([CountriesEffects]),
    StoreModule.forFeature('countriesState', countriesReducer),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    CountriesService
  ]
})
export class RegionModule { }
