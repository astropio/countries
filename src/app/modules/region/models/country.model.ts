export interface CountryModel{
    name: string;
    cca3: string;
}