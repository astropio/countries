import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CountryState } from '../country/store/country.state';
import { ImageService } from '../shared/services/image.service';
import { CountryModel } from './models/country.model';
import { ShowAllAction } from './store/region.actions';
import { getAllCountries } from './store/region.selectors';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss']
})
export class RegionComponent implements OnInit {

  region: string = '';

  countries$: Observable<CountryModel[]>;


  constructor(
    private route: ActivatedRoute,
    private imageService: ImageService,
    private store: Store<CountryState>
    ){
      this.countries$ = store.select(getAllCountries);
    }


  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.region = params['name'] || '';
      this.store.dispatch(ShowAllAction({payload: {region: params['name']}}))
    });

  }
  
  getImageLink(country: CountryModel):string{
    return this.imageService.getImageLink(country.cca3);
  }

}
