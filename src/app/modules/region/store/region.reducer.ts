import { Action, createReducer, on } from "@ngrx/store";
import { CountriesState } from "./region.state";
import * as fromActions from './region.actions'

export const initialState: CountriesState = {countries: [], message: ''};

export const countriesReducer = createReducer(
    initialState,
    on(fromActions.ShowAllSuccessAction, (state, {payload}) => ({countries: payload.countries, message: 'Success'})),
);
