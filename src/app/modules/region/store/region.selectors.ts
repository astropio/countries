import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CountriesState } from "./region.state";

export const getCountriesState = createFeatureSelector<CountriesState>('countriesState');

export const getAllCountries = createSelector(
    getCountriesState,
    (state: CountriesState) => state.countries
);

