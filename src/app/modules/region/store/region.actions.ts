import { createAction, props } from "@ngrx/store";
import { CountryModel } from "../models/country.model";

export const ShowAllAction = createAction('[Countries] Show All', props<{payload: {region: string}}>());
export const ShowAllSuccessAction = createAction('[Countries] Show Success', props<{ payload: {countries: CountryModel[]}}>());
