import { CountryModel } from "../models/country.model";

export interface AppState {
	countryState: CountriesState;
}

export interface CountriesState {
	countries: CountryModel[];
	message: any;
} 