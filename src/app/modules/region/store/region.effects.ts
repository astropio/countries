import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, EMPTY, map, mergeMap, of, switchMap, tap } from "rxjs";
import { CountryModel } from "../models/country.model";
import { CountriesService } from "../services/countries.service";
import * as fromActions from "./region.actions"

@Injectable()
export class CountriesEffects{

    countriesCache: {
        [key: string]: CountryModel[]
    } = {}

    constructor(
        private actions$: Actions,
        private countriesService: CountriesService
      ) { }
    
    loadAllCountries = createEffect(() => 
        this.actions$.pipe(
            ofType(fromActions.ShowAllAction),
            switchMap((action) => {
                if(!this.countriesCache[action.payload.region])
                    return this.countriesService.getCountries(action.payload.region)
                    .pipe(map(data => data.map(country => {return {name: country.name.common, cca3: country.cca3}})))
                    .pipe(
                        tap(data => this.countriesCache[action.payload.region] = data),
                        map(data => fromActions.ShowAllSuccessAction({payload: {countries: data}}))
                    )
                else
                    return of(fromActions.ShowAllSuccessAction({payload: {countries: this.countriesCache[action.payload.region]}}))
            }
            )
        )
    )
  
}