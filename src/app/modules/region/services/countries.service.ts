import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { debounceTime, map, Observable, shareReplay, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CountryModel } from '../models/country.model';

@Injectable()
export class CountriesService {


  constructor(private http: HttpClient ) { }

  getCountries(region: string): Observable<{name: {common: string, native: string}, cca3: string}[]>{
    return this.http
      .get<{name: {common: string, native: string}, cca3: string}[]>(environment.api + 'region/' + region + '?fields=name,cca3')
      .pipe(tap(console.log))

  }



}
