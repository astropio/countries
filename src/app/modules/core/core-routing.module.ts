import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', 
    loadChildren: () => import('../main/main.module').then(m => m.MainModule),
  },
  {
    path: 'region/:name',
    loadChildren: () => import('../region/region.module').then(m => m.RegionModule)
  },
  {
    path: 'country/:name/:code',
    loadChildren: () => import('../country/country.module').then(m => m.CountryModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
