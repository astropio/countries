import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  baseRoute: boolean = true;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((e) => {
      let url = this.router.url.split('/');
      this.baseRoute = url[1] ? false : true;
    })


  }

  getBack(){
    let url = this.router.url.split('/');
    console.log(url);
    switch(url[1]){
      case 'region':
        this.router.navigate([''],);
        break;
      case 'country':
        this.router.navigate(['region',url[2]]);
        break;
    }
    
  }
}
