import { Component, EventEmitter, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Output('showDialog') showDialog: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  OMnie(){
    console.log('omnie')

    this.showDialog.emit(true);
  }

}
