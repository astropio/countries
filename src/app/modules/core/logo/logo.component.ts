import { Component } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent {

  logoList = [
    "angular.svg",
    "angular_white.svg",
    "angular_black.svg",
  ]

  url = "assets/images/";
  i = 0;

  link = this.url + this.logoList[0];


  constructor() { }

  change(){
    this.link = this.url + this.logoList[(++this.i)%3];
  }

}
